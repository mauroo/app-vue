  var app=Vue.createApp({

    async mounted() {
      // chiamo il metodo reloadTodos che si occupa
      // di caricare l'elenco dei todo ed assegnarlo
      // alla proprietà "todos"
      this.loading = true;
      this.reloadTodos();
    },
    methods: {
      async reloadTodos(){
        var todoResponse = await fetch('http://localhost:8000/api/todos');
        var todoJson = await todoResponse.json();
        // assegno al "data" todos l'array di todo
        // che viene tornato dalla fetch
        this.todos = todoJson;
        this.loading =false;
      },
      // questo viene chiamato da @click "aggiungi"
      async addTodo(){
        await fetch('http://localhost:8000/api/todos',{ 
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            description: this.newTodoDescription
          })
        });
        this.reloadTodos();
      },
      async deleteTodo(idTodo){
        await fetch('http://localhost:8000/api/todos/'+idTodo, {
          method: 'DELETE',
          headers: { 'Content-Type': 'application/json' }
        });
        // dopo aver eliminato il todo ricarico la lista
        this.reloadTodos();
      },
      /* questo metodo serve a cambiare il campo done del todo nel DB
      *  quindi se done=1 deve mettere 0 e viceversa 
      */
      async toggleTodo(todo){
        await fetch('http://localhost:8000/api/todos/'+todo.id, {
          method: 'PUT',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            description: this.description,
            done: todo.done == 1 ? 0 : 1 // cambio il valore di done
          })
        });
        this.reloadTodos();
      }
    },
      template: /** alt + 96 (del tastierino) */` 
        <h1>TODO APP</h1>
        <input type="text" v-model="newTodoDescription" />  
        <button @click="addTodo">Aggiungi</button>
        
        <svg v-if="loading" version="1.1" id="L2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
        <circle fill="none" stroke="#000" stroke-width="4" stroke-miterlimit="10" cx="50" cy="50" r="48"/>
        <line fill="none" stroke-linecap="round" stroke="#000" stroke-width="4" stroke-miterlimit="10" x1="50" y1="50" x2="85" y2="50.5">
        <animateTransform 
            attributeName="transform" 
            dur="2s"
            type="rotate"
            from="0 50 50"
            to="360 50 50"
            repeatCount="indefinite" />
        </line>
        <line fill="none" stroke-linecap="round" stroke="#000" stroke-width="4" stroke-miterlimit="10" x1="50" y1="50" x2="49.5" y2="74">
        <animateTransform 
            attributeName="transform" 
            dur="15s"
            type="rotate"
            from="0 50 50"
            to="360 50 50"
            repeatCount="indefinite" />
        </line>
        </svg>

        <div v-if="todos.lenght <= 0 && loading == false">Non ci sono todos</div>
        
        <ul>
          <li v-for="todo in todos">  
            <i class="mdi mdi-delete" @click="deleteTodo(todo.id)"></i>
            <input type="checkbox" :checked="todo.done==1" @click="toggleTodo(todo)" />
            <span>{{ todo.description }}</span>
          </li>
        </ul>
        `,
      data() {
        return {
          loading: false,
          // questa proprietà la collego al valore dell'input
          todos: [], // => nuovo Array()
        }
      }
  });
  app.mount('#app');
