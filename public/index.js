// questa funzione mi aggiunge il mio bel todo
// dentro il db del prof Masetti all'indirizzo 10. bla bla
// usando il mio web server lumen che fa tante cose belle
async function addTodo(todoDescription) {
    // devo fare una chiamata HTTP post
    // che simula quello che fa postman 
    var newTodo = {
        description: todoDescription
    }
    await fetch('http://localhost:8000/api/todos', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(newTodo)
    });
    // dopo aver aggiunto il todo ricarico la lista
    reloadTodos();
}
// questa riceve l'id del todo ed esegue la chiamata
// HTTP => API/TODOS/{id} [DELETE]
async function deleteTodo(id){
    await fetch('http://localhost:8000/api/todos/'+id, {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' }
    });
    // dopo aver eliminato il todo ricarico la lista
    reloadTodos();
}
async function reloadTodos() {
    // ricarico i todos via HTTP con la funzione fetch
    var todosResponse = await fetch('http://localhost:8000/api/todos');
    var todosJson = await todosResponse.json();
    // recupero la lista UL in javascript
    var ulList = document.getElementById('todosList');
    ulList.innerHTML = "";
    // itero gli elementi e li aggiungo all'UL
    for (var i = 0; i < todosJson.length; i++) {
        // creo una variabile con l'elemento i-esimo della lista (il singolo todo)
        var todo = todosJson[i];
        // creo il tag LI
        var liElement = document.createElement('li');
        var spanElement = document.createElement('span');
        var removeButton = document.createElement('button');
        removeButton.innerText = "X";
        // creo una nuova funzione con il parametro id bindato
        // in modo da richiamare la funzione deleteTodo con quell'id fisso
        var removeThisTodo=deleteTodo.bind(null,todo.id);
        // aggiungiamo l'evento click sul bottone removeButton
        // che quando clicca esegue la funzione removeTodo
        removeButton.addEventListener('click',removeThisTodo)
        // metto la descrizione come contenuto del LI
        spanElement.textContent = '#' + todo.id + ' ' + todo.description;
        spanElement.textContent += ' (';
        if (todo.done) {
            spanElement.textContent += 'Completato';
        }
        else {
            spanElement.textContent += 'Da completare';
        }
        spanElement.textContent += ')';
        // aggiungiamo il bottone
        liElement.appendChild(removeButton);
        // il testo del todo poi
        liElement.appendChild(spanElement);
        // lo appendiamo alla lista
        ulList.appendChild(liElement);
    }

}
// da qui parte la nostra app JS
// la parola async dice al browser che
// all'interno di questa funzione viene usata
// un'operazione asincrona (await)
async function main() {
    console.log('running main');
    // prendo il tag div con id app
    var divApp = document.getElementById("app");
    // creo un tag title
    var title = document.createElement('h1');
    title.textContent = "TODO APP";
    // aggiungo il tag all'interno del div
    divApp.appendChild(title);
    // prendo i todos via HTTP con la funzione fetch
    // N.B la parola await permette di rendere sincrona 
    // un'operazione asincrona
    var todosResponse = await fetch('http://localhost:8000/api/todos');
    var todosJson = await todosResponse.json();
    console.log(todosJson);
    // aggiungo una casella di testo
    var textInput = document.createElement('input');
    textInput.placeholder = 'Inserisci un nuovo todo';
    // aggiungo un id all'input perché mi serve più tardi per essere
    // recuperato
    textInput.id = 'addTodoInput';
    divApp.appendChild(textInput);
    // aggiungo il bottone aggiungi ahah
    var addButton = document.createElement('button');
    addButton.textContent = 'Aggiungimi';
    divApp.appendChild(addButton);
    // attacho l'evento click del bottone
    addButton.addEventListener('click', function () {
        // recupero la descrizione del nuovo todo
        var todoDesc = document.getElementById('addTodoInput').value;
        // uso la funziona addTodo che definisco sopra
        addTodo(todoDesc);
    })
    // creo la lista UL in javascript
    var ulList = document.createElement('ul');
    ulList.id = "todosList";
    // aggiungo la lista alla fine del container div#app
    divApp.appendChild(ulList);

    reloadTodos();
}

//main();

// associo l'evento DOMContentLoaded
// alla funzione main
document.addEventListener("DOMContentLoaded", function () {
    main();
});

//document.addEventListener("DOMContentLoaded",main);

